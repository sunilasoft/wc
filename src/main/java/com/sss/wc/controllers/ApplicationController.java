/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sss.wc.controllers;

import com.sss.wc.entity.Patient;
import com.sss.wc.facades.PatientFacade;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author nilukagun
 */
@Named(value = "applicationController")
@ApplicationScoped
public class ApplicationController {

    @EJB
    PatientFacade patientFacade;
    
    /**
     * Creates a new instance of ApplicationController
     */
    public ApplicationController() {
    }
    
    public void generateNewClinicNumber(Patient p) {
        String j = "select count(p) "
                + " from Patient p";
        Long ptCount = getPatientFacade().countBySql(j);
        p.setLongReferenceNumber(ptCount);
        String clinicNo = p.getRegisteredInstitute().getCode() + "/" + ptCount+1;
        p.setReferenceNumber(clinicNo);
        //TODO: Check wether this registration number exists
    }

    public PatientFacade getPatientFacade() {
        return patientFacade;
    }
    
    
    
    
}
