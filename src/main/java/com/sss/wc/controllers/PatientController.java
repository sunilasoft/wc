package com.sss.wc.controllers;

import com.sss.wc.entity.Patient;
import com.sss.wc.controllers.util.JsfUtil;
import com.sss.wc.controllers.util.JsfUtil.PersistAction;
import com.sss.wc.entity.Encounter;
import com.sss.wc.entity.Wound;
import com.sss.wc.entity.EncounterItem;
import com.sss.wc.entity.Item;
import com.sss.wc.entity.Resource;
import com.sss.wc.enums.DoseUnit;
import com.sss.wc.enums.DurationUnit;
import com.sss.wc.enums.EncounterItemType;
import com.sss.wc.facades.EncounterFacade;
import com.sss.wc.facades.WoundFacade;
import com.sss.wc.facades.EncounterItemFacade;
import com.sss.wc.facades.PatientFacade;
import com.sss.wc.facades.ResourceFacade;
import java.io.IOException;
import java.io.InputStream;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import org.apache.poi.util.IOUtils;
import org.primefaces.event.CaptureEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.UploadedFile;

@Named("patientController")
@SessionScoped
public class PatientController implements Serializable {

    @Inject
    WebUserController webUserController;
    @Inject
    WoundController woundController;
    @Inject
    EncounterController encounterController;
    @Inject
    EncounterItemController encounterItemController;
    @Inject
    ResourceController resourceController;
    @Inject
    ApplicationController applicationController;

    @EJB
    PatientFacade ejbFacade;
    @EJB
    WoundFacade woundFacade;
    @EJB
    EncounterFacade encounterFacade;
    @EJB
    EncounterItemFacade encounterItemFacade;
    @EJB
    ResourceFacade resourceFacade;

    private Patient selected;
    Wound selectedWound;
    Encounter selectedEncounter;
    EncounterItem selectedEncounterItem;
    Resource selectedResource;

    String searchText;
    private UploadedFile file;
    private int encounterPhotoIndex = 0;
    private int woundPhotoIndex = 0;
    Item dressing;
    Item investigation;
    Item medicine;
    Long duration;
    DurationUnit durationUnit;
    Double dose;
    DoseUnit doseUnit;

    private List<Patient> items = null;
    List<Wound> selectedWounds;
    List<Encounter> selectedEncounters;
    List<EncounterItem> selectedEncounterItems;
    List<Resource> selectedWoundImages;
    List<Resource> selectedEncounterImages;

    private Resource firstImage;
    private Resource lastImage;
    
    
    
    public PatientController() {
    }

    
    
    public void patientDobChanged(SelectEvent event) {
        getSelected().setAge(calculateAge(getSelected().getDateOfBirth(), new Date()));
    }

    public String calculateAge(Date dob, Date toDate) {
        if (dob == null || toDate == null) {
            return "";
        }
        long ageInDays;
        ageInDays = (toDate.getTime() - dob.getTime()) / (1000 * 60 * 60 * 24);
        if (ageInDays < 0) {
            return "";
        }
        String strAgr = "Age is ";
        if (ageInDays < 60) {
            strAgr += " days";
        } else if (ageInDays < 366) {
            strAgr += (ageInDays / 30) + " months";
        } else {
            strAgr += (ageInDays / 365) + " years";
        }
        return strAgr;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }
    
    public String deletePhoto() {
        if(selectedResource==null){
            JsfUtil.addErrorMessage("No photo to delete");
            return "";
        }
        getResourceFacade().remove(selectedResource);
        JsfUtil.addSuccessMessage("Photo Deleted");
        return "/patient/patient";
    }

    public void savePhoto() {
        InputStream in;
        if (file == null || "".equals(file.getFileName())) {
            JsfUtil.addErrorMessage("Please select an image");
            return;
        }
        if (file == null) {
            JsfUtil.addErrorMessage("Please select an image");
            return;
        }
        if (getSelected() == null || getSelected().getId() == null || getSelected().getId() == 0) {
            JsfUtil.addErrorMessage("Please select a patient");
            return;
        }
        if (getSelectedWound() == null || getSelectedWound().getId() == null || getSelectedWound().getId() == 0) {
            JsfUtil.addErrorMessage("Please select an Wound");
            return;
        }
        if(selectedEncounter.getId()==null){
            getEncounterFacade().create(selectedEncounter);
        }else{
            getEncounterFacade().edit(selectedEncounter);
        }
        if(selectedWound.getId()==null){
            getWoundFacade().create(selectedWound);
        }else{
            getWoundFacade().edit(selectedWound);
        }
        try {
            in = getFile().getInputstream();
            Resource ei = new Resource();
            ei.setFileName(file.getFileName());
            ei.setFileType(file.getContentType());
            ei.setBaImage(IOUtils.toByteArray(in));
            ei.setEncounter(selectedEncounter);
            ei.setWound(selectedWound);
            getResourceFacade().create(ei);
            selectedWoundImages = null;
            selectedEncounterImages = null;
            getSelectedWoundImages();
            getSelectedEncounterImages();
            JsfUtil.addSuccessMessage("Image Added");
        } catch (IOException e) {
            System.out.println("Error " + e.getMessage());
        }

    }

    public void addDressing() {
        if (selectedEncounter == null) {
            JsfUtil.addErrorMessage("Encounter?");
            return;
        }
        if (dressing == null) {
            JsfUtil.addErrorMessage("Dressing ?");
            return;
        }
        if(selectedEncounter.getId()==null){
            getEncounterFacade().create(selectedEncounter);
        }else{
            getEncounterFacade().edit(selectedEncounter);
        }
        EncounterItem ei = new EncounterItem();
        ei.setEncounter(selectedEncounter);
        ei.setEncounterItemType(EncounterItemType.Wound_dressing);
        ei.setItem(dressing);
        getEncounterItemFacade().create(ei);
        setSelectedEncounterItems(null);
        getSelectedEncounterItems();
        dressing = null;
    }

    public void addInvestigation() {
        if (selectedEncounter == null) {
            JsfUtil.addErrorMessage("Encounter?");
            return;
        }
        if (investigation == null) {
            JsfUtil.addErrorMessage("Dressing ?");
            return;
        }
        if(selectedEncounter.getId()==null){
            getEncounterFacade().create(selectedEncounter);
        }else{
            getEncounterFacade().edit(selectedEncounter);
        }
        EncounterItem ei = new EncounterItem();
        ei.setEncounter(selectedEncounter);
        ei.setEncounterItemType(EncounterItemType.Investigation);
        ei.setItem(investigation);
        ei.setDose(dose);
        getEncounterItemFacade().create(ei);
        setSelectedEncounterItems(null);
        getSelectedEncounterItems();
        investigation = null;
        dose=null;
        
    }

    public void addMedicine() {
        if (selectedEncounter == null) {
            JsfUtil.addErrorMessage("Encounter?");
            return;
        }
        if (medicine == null) {
            JsfUtil.addErrorMessage("Dressing ?");
            return;
        }
        if(selectedEncounter.getId()==null){
            getEncounterFacade().create(selectedEncounter);
        }else{
            getEncounterFacade().edit(selectedEncounter);
        }
        EncounterItem ei = new EncounterItem();
        ei.setEncounter(selectedEncounter);
        ei.setEncounterItemType(EncounterItemType.Oral_medications);
        ei.setItem(medicine);
        ei.setDose(dose);
        ei.setDoseUnit(doseUnit);
        ei.setDuration(duration);
        ei.setDurationUnit(durationUnit);
        getEncounterItemFacade().create(ei);
        setSelectedEncounterItems(null);
        getSelectedEncounterItems();
        medicine = null;
        dose = null;
        doseUnit = null;
        duration = null;
        durationUnit = null;
    }

    public void removeEncounterItem() {
        if (selectedEncounterItem == null) {
            JsfUtil.addErrorMessage("What to remove ?");
            return;
        }
        getEncounterItemFacade().remove(selectedEncounterItem);
        JsfUtil.addSuccessMessage("Removed");
        setSelectedEncounterItems(null);
        getSelectedEncounterItems();
    }

    public void capturePhoto(CaptureEvent captureEvent) {
        if (file == null || "".equals(file.getFileName())) {
            JsfUtil.addErrorMessage("Please select an image");
            return;
        }
        if (file == null) {
            JsfUtil.addErrorMessage("Please select an image");
            return;
        }
        if (getSelected() == null || getSelected().getId() == null || getSelected().getId() == 0) {
            JsfUtil.addErrorMessage("Please select a patient");
            return;
        }
        if (getSelectedWound() == null || getSelectedWound().getId() == null || getSelectedWound().getId() == 0) {
            JsfUtil.addErrorMessage("Please select an Wound");
            return;
        }
        try {
            Resource ei = new Resource();
            ei.setFileName("Patient_Image_" + getSelected().getNameOfPatient() + "_" + getSelectedWound().getName() + "_" + getSelectedEncounter().getEncounterDateTime() + ".png");
            ei.setFileType("image/png");
            ei.setBaImage(captureEvent.getData());
            ei.setEncounter(selectedEncounter);
            ei.setWound(selectedWound);
            getResourceFacade().create(ei);
            selectedWoundImages = null;
            selectedEncounterImages = null;
            getSelectedWoundImages();
            getSelectedEncounterImages();
            JsfUtil.addSuccessMessage("Photo captured from webcam.");
        } catch (Exception e) {
            System.out.println("Error " + e.getMessage());
        }
    }

    public Resource nextWoundPhoto() {
        if (getSelectedWoundImages() == null) {
            return null;
        }
        if (getSelectedWoundImages().isEmpty()) {
            return null;
        }
        if (woundPhotoIndex >= getSelectedWoundImages().size()) {
            woundPhotoIndex = 0;
        }
        Resource ei = getSelectedWoundImages().get(woundPhotoIndex);
        woundPhotoIndex++;
        return ei;
    }

    public Resource nextEncounterPhoto() {
        if (getSelectedEncounterImages() == null) {
            return null;
        }
        if (getSelectedEncounterImages().isEmpty()) {
            return null;
        }
        if (encounterPhotoIndex >= getSelectedEncounterImages().size()) {
            encounterPhotoIndex = 0;
        }
        Resource ei = getSelectedEncounterImages().get(encounterPhotoIndex);
        encounterPhotoIndex++;
        return ei;
    }

    public void listAllPatients() {
        String j = "select p "
                + " from Patient p order by p.id";
        items = getFacade().findBySQL(j);
    }

    public void searchPatients() {
        if (searchText.trim().equals("")) {
            JsfUtil.addErrorMessage("Please enter few letters to search.");
            return;
        }
        String j = "select p "
                + " from Patient p "
                + " where lower(p.nameOfPatient) like :qry "
                + " or lower(p.referenceNumber) like :qry "
                + " or lower(p.address) like :qry "
                + " or p.contactNumber like :qry ";
        Map m = new HashMap();
        m.put("qry", "%" + searchText.toLowerCase() + "%");
        items = getFacade().findBySQL(j, m);
        searchText = "";
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public WebUserController getWebUserController() {
        return webUserController;
    }

    public WoundController getWoundController() {
        return woundController;
    }

    public String toSearchPatient() {
        items = new ArrayList<Patient>();
        return "/patient/search";
    }

    public String toViewPatient() {
        return "/patient/patient";
    }

    public String toViewWound() {
        return "/patient/wound";
    }

    public String toViewEncounter() {
        return "/patient/encounter";
    }

    public String toAddNewPatient() {
        selected = new Patient();
        selected.setDateOfRegistration(new Date());
        if (getWebUserController().getLoggedUser() != null) {
            selected.setRegisteredInstitute(getWebUserController().getLoggedUser().getInstitute());
            selected.setRegisteredDepartment(getWebUserController().getLoggedUser().getDepartment());
        }

        selectedEncounter = new Encounter();
        selectedEncounter.setEncounterDate(new Date());
        selectedEncounter.setEncounterDateTime(new Date());
        selectedEncounter.setPatient(selected);

        selectedWound = null;

        selectedEncounters = null;
        selectedWounds = null;

        selectedEncounterImages = null;
        selectedWoundImages = null;

        selectedResource = null;

        firstImage = null;
        lastImage = null;
        
        return "/patient/patient";
    }

    public String savePatient() {
        if (selected == null) {
            JsfUtil.addErrorMessage("Nothing to save");
            return "";
        }
        if (selected.getReferenceNumber() == null || selected.getReferenceNumber().trim().equals("")) {
            applicationController.generateNewClinicNumber(selected);
        }
        if (selected.getId() == null) {
            getFacade().create(selected);
            JsfUtil.addSuccessMessage("Created");
        } else {
            getFacade().edit(selected);
            JsfUtil.addSuccessMessage("Updated");
        }
        if (selectedEncounter != null) {
            if (selectedEncounter.getId() == null) {
                getEncounterFacade().create(selectedEncounter);
            }
        }
        return "";
    }

    public String saveWound() {
        if (selectedWound == null) {
            JsfUtil.addErrorMessage("Nothing to save");
            return "";
        }
        selectedWound.setName(selectedWound.getLaterality() + " " + selectedWound.getUlcerType());
        if (selectedWound.getId() == null) {
            getWoundFacade().create(selectedWound);
            JsfUtil.addSuccessMessage("Saved");
        } else {
            getWoundFacade().edit(selectedWound);
            JsfUtil.addSuccessMessage("Updated");
        }
        return "";
    }

    public String saveEncounter() {
        if (selectedEncounter == null) {
            JsfUtil.addErrorMessage("Nothing to save");
            return "";
        }
        if (selectedEncounter.getId() == null) {
            getEncounterFacade().create(selectedEncounter);
            JsfUtil.addSuccessMessage("Saved");
        } else {
            getEncounterFacade().edit(selectedEncounter);
            JsfUtil.addSuccessMessage("Updated");
        }
        return "";
    }

    public String saveAndNewWound() {
        savePatient();
        selectedWound = new Wound();
        selectedWound.setPatient(selected);
        return "/patient/wound";
    }
    
    
    public String saveAndNewVisit() {
        savePatient();
        selectedEncounter = new Encounter();
        selectedEncounter.setPatient(selected);
        selectedEncounter.setEncounterDate(new Date());
        selectedEncounter.setEncounterDateTime(new Date());
        return "/patient/encounter";
    }

    public Patient getSelected() {
        return selected;
    }

    
    public void setSelected(Patient selected) {
        setSelectedEncounter(null);
        setSelectedWound(null);
        setSelectedEncounters(null);
        setSelectedWounds(null);
        setSelectedEncounterItems(null);
        setSelectedWoundImages(null);
        setSelectedEncounterImages(null);

        this.selected = selected;

        getSelectedWounds();
        getSelectedEncounters();
        if (!getSelectedWounds().isEmpty()) {
            setSelectedWound(getSelectedWounds().get(0));
            getSelectedWoundImages();
        }
        if (!getSelectedEncounters().isEmpty()) {
            setSelectedEncounter(getSelectedEncounters().get(0));
            getSelectedEncounterItems();
            getSelectedEncounterImages();
        }

    }

    public List<Wound> getSelectedWounds() {
        if (selectedWounds == null) {
            String j = "select e from Wound e "
                    + " where e.patient.id=:ptid "
                    + " order by e.id desc";
            Map m = new HashMap();
            m.put("ptid", selected.getId());
            selectedWounds = getWoundFacade().findBySQL(j, m);
        }
        return selectedWounds;
    }

    public void setSelectedWounds(List<Wound> selectedWounds) {
        this.selectedWounds = selectedWounds;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private PatientFacade getFacade() {
        return ejbFacade;
    }

    public Patient prepareCreate() {
        selected = new Patient();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/BundlePt").getString("PatientCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/BundlePt").getString("PatientUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/BundlePt").getString("PatientDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Patient> getItems() {
        if (items == null) {
            String j = "select p from Patient p";
            items = getFacade().findBySQL(j);
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/BundlePt").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/BundlePt").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Patient getPatient(java.lang.Long id) {
        return getFacade().find(id);
    }

    public List<Patient> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Patient> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    public ApplicationController getApplicationController() {
        return applicationController;
    }

    public EncounterItemFacade getEncounterItemFacade() {
        return encounterItemFacade;
    }

    public Wound getSelectedWound() {
        return selectedWound;
    }

    public void setSelectedWound(Wound selectedWound) {
        this.selectedWound = selectedWound;
        setSelectedWoundImages(null);
        setSelectedResource(null);
    }

    public EncounterItem getSelectedEncounterItem() {
        return selectedEncounterItem;
    }

    public void setSelectedEncounterItem(EncounterItem selectedEncounterItem) {
        this.selectedEncounterItem = selectedEncounterItem;
    }

    public PatientFacade getEjbFacade() {
        return ejbFacade;
    }

    public void setEjbFacade(PatientFacade ejbFacade) {
        this.ejbFacade = ejbFacade;
    }

    public WoundFacade getWoundFacade() {
        return woundFacade;
    }

    public void setWoundFacade(WoundFacade woundFacade) {
        this.woundFacade = woundFacade;
    }

    public List<EncounterItem> getSelectedEncounterItemsDressings() {
        List<EncounterItem> tis = new ArrayList<EncounterItem>();
        for (EncounterItem tei : getSelectedEncounterItems()) {
            if (tei.getEncounterItemType() == EncounterItemType.Wound_dressing) {
                tis.add(tei);
            }
        }
        return tis;
    }

    public List<EncounterItem> getSelectedEncounterItemsInvestigations() {
        List<EncounterItem> tis = new ArrayList<EncounterItem>();
        for (EncounterItem tei : getSelectedEncounterItems()) {
            if (tei.getEncounterItemType() == EncounterItemType.Investigation) {
                tis.add(tei);
            }
        }
        return tis;
    }
    
     public List<EncounterItem> getSelectedEncounterItemsMedicines() {
        List<EncounterItem> tis = new ArrayList<EncounterItem>();
        for (EncounterItem tei : getSelectedEncounterItems()) {
            if (tei.getEncounterItemType() == EncounterItemType.Oral_medications) {
                tis.add(tei);
            }
        }
        return tis;
    }

    public List<EncounterItem> getSelectedEncounterItems() {
        if (selectedEncounter == null || selectedEncounter.getId()==null) {
            selectedEncounterItems = new ArrayList<EncounterItem>();
        } else {
            if (selectedEncounterItems == null) {
                String j = "select i "
                        + " from EncounterItem i "
                        + " where i.encounter.id=:enid"
                        + " order by i.id";
                Map m = new HashMap();
                m.put("enid", selectedEncounter.getId());
                selectedEncounterItems = getEncounterItemFacade().findBySQL(j, m);
            } else {
                System.out.println("selectedEncounterImages is NOT null");
            }
        }
        return selectedEncounterItems;
    }

    public EncounterController getEncounterController() {
        return encounterController;
    }

    public EncounterItemController getEncounterItemController() {
        return encounterItemController;
    }

    public ResourceController getResourceController() {
        return resourceController;
    }

    public EncounterFacade getEncounterFacade() {
        return encounterFacade;
    }

    public ResourceFacade getResourceFacade() {
        return resourceFacade;
    }

    public void setSelectedEncounterItems(List<EncounterItem> selectedEncounterItems) {
        this.selectedEncounterItems = selectedEncounterItems;
    }

    public Encounter getSelectedEncounter() {
        return selectedEncounter;
    }

    public void setSelectedEncounter(Encounter selectedEncounter) {
        this.selectedEncounter = selectedEncounter;
        setSelectedEncounterItems(null);
        setSelectedEncounterImages(null);
        setSelectedEncounterItem(null);
        setSelectedResource(null);
    }

    public Resource getSelectedResource() {
        return selectedResource;
    }

    public void setSelectedResource(Resource selectedResource) {
        this.selectedResource = selectedResource;
    }

    public List<Encounter> getSelectedEncounters() {
        if (selectedEncounters == null) {
            String j = "select e from Encounter e "
                    + " where e.patient.id=:ptid "
                    + " order by e.id desc";
            Map m = new HashMap();
            m.put("ptid", selected.getId());
            selectedEncounters = getEncounterFacade().findBySQL(j, m);
        }
        return selectedEncounters;
    }

    public void setSelectedEncounters(List<Encounter> selectedEncounters) {
        this.selectedEncounters = selectedEncounters;
    }

    public List<Resource> getSelectedWoundImages() {
        if (selectedWound == null) {
            selectedWoundImages = null;
        } else {
            if (selectedWoundImages == null) {
                String j = "select i "
                        + " from Resource i "
                        + " where i.wound.id=:enid"
                        + " order by i.id";
                Map m = new HashMap();
                m.put("enid", selectedWound.getId());
                selectedWoundImages = getResourceFacade().findBySQL(j, m);
                if(selectedWoundImages !=null && !selectedWoundImages.isEmpty()){
                    firstImage = selectedWoundImages.get(0);
                    if(selectedWoundImages.size()>=2){
                        lastImage = selectedWoundImages.get(selectedWoundImages.size()-1);
                    }
                }
            } else {
                System.out.println("selectedEncounterImages is NOT null");
            }
        }
        return selectedWoundImages;
    }

    public void setSelectedWoundImages(List<Resource> selectedWoundImages) {
        this.selectedWoundImages = selectedWoundImages;
    }

    public List<Resource> getSelectedEncounterImages() {
        if (selectedEncounter == null) {
            selectedEncounterImages = null;
        } else {
            if (selectedEncounterImages == null) {
                String j = "select i "
                        + " from Resource i "
                        + " where i.encounter.id=:enid"
                        + " order by i.id";
                Map m = new HashMap();
                m.put("enid", selectedEncounter.getId());
                selectedEncounterImages = getResourceFacade().findBySQL(j, m);
            } else {
                System.out.println("selectedEncounterImages is NOT null");
            }
        }
        return selectedEncounterImages;
    }

    public void setSelectedEncounterImages(List<Resource> selectedEncounterImages) {
        this.selectedEncounterImages = selectedEncounterImages;
    }

    public Item getDressing() {
        return dressing;
    }

    public void setDressing(Item dressing) {
        this.dressing = dressing;
    }

    public Item getInvestigation() {
        return investigation;
    }

    public void setInvestigation(Item investigation) {
        this.investigation = investigation;
    }

    public Item getMedicine() {
        return medicine;
    }

    public void setMedicine(Item medicine) {
        this.medicine = medicine;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public DurationUnit getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(DurationUnit durationUnit) {
        this.durationUnit = durationUnit;
    }

    public Double getDose() {
        return dose;
    }

    public void setDose(Double dose) {
        this.dose = dose;
    }

    public DoseUnit getDoseUnit() {
        return doseUnit;
    }

    public void setDoseUnit(DoseUnit doseUnit) {
        this.doseUnit = doseUnit;
    }

    public int getEncounterPhotoIndex() {
        return encounterPhotoIndex;
    }

    public void setEncounterPhotoIndex(int encounterPhotoIndex) {
        this.encounterPhotoIndex = encounterPhotoIndex;
    }

    public int getWoundPhotoIndex() {
        return woundPhotoIndex;
    }

    public void setWoundPhotoIndex(int woundPhotoIndex) {
        this.woundPhotoIndex = woundPhotoIndex;
    }

    public Resource getFirstImage() {
        return firstImage;
    }

    public void setFirstImage(Resource firstImage) {
        this.firstImage = firstImage;
    }

    public Resource getLastImage() {
        return lastImage;
    }

    public void setLastImage(Resource lastImage) {
        this.lastImage = lastImage;
    }

    @FacesConverter(forClass = Patient.class)
    public static class PatientControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            PatientController controller = (PatientController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "patientController");
            return controller.getPatient(getKey(value));
        }

        java.lang.Long getKey(String value) {
            java.lang.Long key;
            key = Long.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Long value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Patient) {
                Patient o = (Patient) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Patient.class.getName()});
                return null;
            }
        }

    }

}
