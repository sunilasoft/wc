/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sss.wc.controllers;

import com.sss.wc.enums.DoseUnit;
import com.sss.wc.enums.DurationUnit;
import javax.faces.bean.ApplicationScoped;
import javax.inject.Named;
import com.sss.wc.enums.Gender;
import com.sss.wc.enums.Intervention;
import com.sss.wc.enums.ItemType;
import com.sss.wc.enums.Laterality;
import com.sss.wc.enums.Onset;
import com.sss.wc.enums.Privilege;
import com.sss.wc.enums.UlcerType;

/**
 *
 * @author User
 */
@Named
@ApplicationScoped
public class EnumController {

    /**
     * Creates a new instance of EnumController
     */
    public EnumController() {
    }

    public Onset[] getOnsets() {
        return Onset.values();
    }

    public Laterality[] getLateralities() {
        return Laterality.values();
    }

    public Laterality[] getLateralitiesWithoutOther() {
        Laterality[] ls = {Laterality.Right, Laterality.Left, Laterality.Bilateral};
        return ls;
    }

    public Gender[] getGenders() {
        return Gender.values();
    }

    public ItemType[] getItemTypes() {
        return ItemType.values();
    }

    public DurationUnit[] getDurationUnits() {
        return DurationUnit.values();
    }

    public DurationUnit[] getDurationDaysOrWeeks() {
        DurationUnit[] tds = {DurationUnit.Days, DurationUnit.Weeks};
        return tds;
    }
    
    
    public UlcerType[] getUlcerTypes() {
        return UlcerType.values();
    }

    public DoseUnit[] getDoseUnits() {
        return DoseUnit.values();
    }

    public Privilege[] getPrivileges() {
        return Privilege.values();
    }
    
    public Intervention[] getInterventions(){
        return Intervention.values();
    }
    
}
