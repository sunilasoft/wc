package com.sss.wc.controllers;



import com.sss.wc.controllers.util.JsfUtil;
import com.sss.wc.controllers.util.JsfUtil.PersistAction;
import com.sss.wc.entity.Item;
import com.sss.wc.enums.ItemType;
import com.sss.wc.facades.ItemFacade;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Named("itemController")
@SessionScoped
public class ItemController implements Serializable {

    @EJB
    private com.sss.wc.facades.ItemFacade ejbFacade;
    private List<Item> items = null;
    private List<Item> dressings = null;
    private List<Item> investigations = null;
    private List<Item> medicines = null;
    
    
    private Item selected;

    public ItemController() {
    }

    public Item getSelected() {
        return selected;
    }

    public List<Item> getDressings() {
        if(dressings==null){
            dressings = fillItems(ItemType.Wound_dressing);
        }
        return dressings;
    }

    public void setDressings(List<Item> dressings) {
        this.dressings = dressings;
    }

    public List<Item> getInvestigations() {
        if(investigations==null){
            investigations = fillItems(ItemType.Investigation);
        }
        return investigations;
    }

    public void setInvestigations(List<Item> investigations) {
        this.investigations = investigations;
    }

    public List<Item> getMedicines() {
        if(medicines==null){
            medicines= fillItems(ItemType.Oral_medications);
        }
        return medicines;
    }

    public void setMedicines(List<Item> medicines) {
        this.medicines = medicines;
    }

    
    
    public void setSelected(Item selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private ItemFacade getFacade() {
        return ejbFacade;
    }

    public Item prepareCreate() {
        selected = new Item();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle1").getString("ItemCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;
            dressings=null;
            medicines=null;
            investigations=null;
// Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle1").getString("ItemUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle1").getString("ItemDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
           dressings=null;
            medicines=null;
            investigations=null;
        }
    }

    public List<Item> getItems() {
        if (items == null) {
            items = fillItems();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle1").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle1").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Item getItem(java.lang.Long id) {
        return getFacade().find(id);
    }

    public List<Item> getItemsAvailableSelectMany() {
        return getItems();
    }

    public List<Item> getItemsAvailableSelectOne() {
        return getItems();
    }

    public List<Item> fillItems(ItemType itemType){
        String j="select i from Item i where i.itemType=:it order by i.name";
        Map m = new HashMap();
        m.put("it", itemType);
        return getFacade().findBySQL(j, m);
    }
    
    public List<Item> fillItems(){
        String j="select i from Item i order by i.name";
        return getFacade().findBySQL(j);
    }
    
    @FacesConverter(forClass = Item.class)
    public static class ItemControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ItemController controller = (ItemController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "itemController");
            return controller.getItem(getKey(value));
        }

        java.lang.Long getKey(String value) {
            java.lang.Long key;
            key = Long.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Long value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Item) {
                Item o = (Item) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Item.class.getName()});
                return null;
            }
        }

    }

}
