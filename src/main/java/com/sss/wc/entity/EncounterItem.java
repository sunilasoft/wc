/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sss.wc.entity;

import com.sss.wc.enums.DoseUnit;
import com.sss.wc.enums.DurationUnit;
import com.sss.wc.enums.EncounterItemType;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author server
 */
@Entity
public class EncounterItem implements Serializable {

    @ManyToOne
    private Wound imageEncounter;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    Encounter encounter;
    @ManyToOne
    Item item;
    @Enumerated(EnumType.STRING)
    EncounterItemType encounterItemType;
    Double dose;
    @Enumerated(EnumType.STRING)
    DoseUnit doseUnit;
    Long duration;
    @Enumerated(EnumType.STRING)
    DurationUnit durationUnit;

    public Double getDose() {
        return dose;
    }

    public void setDose(Double dose) {
        this.dose = dose;
    }

    public DoseUnit getDoseUnit() {
        return doseUnit;
    }

    public void setDoseUnit(DoseUnit doseUnit) {
        this.doseUnit = doseUnit;
    }

    public DurationUnit getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(DurationUnit durationUnit) {
        this.durationUnit = durationUnit;
    }
    
    

    public Wound getImageEncounter() {
        return imageEncounter;
    }

    public void setImageEncounter(Wound imageEncounter) {
        this.imageEncounter = imageEncounter;
    }

    public Encounter getEncounter() {
        return encounter;
    }

    public void setEncounter(Encounter encounter) {
        this.encounter = encounter;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public EncounterItemType getEncounterItemType() {
        return encounterItemType;
    }

    public void setEncounterItemType(EncounterItemType encounterItemType) {
        this.encounterItemType = encounterItemType;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EncounterItem)) {
            return false;
        }
        EncounterItem other = (EncounterItem) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sss.wc.entity.EncounterItem[ id=" + id + " ]";
    }

}
