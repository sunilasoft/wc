package com.sss.wc.entity;

import com.sss.wc.enums.DurationUnit;
import com.sss.wc.enums.Laterality;
import com.sss.wc.enums.Onset;
import com.sss.wc.enums.UlcerType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.Transient;

/**
 *
 * @author User
 */
@Entity
public class Wound implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    Patient patient;
    String name;
    @Temporal(javax.persistence.TemporalType.DATE)
    Date createdDate;
    @Temporal(javax.persistence.TemporalType.DATE)
    Date inactivatedDate;
    boolean active = true;

    @Enumerated(EnumType.STRING)
    Laterality laterality;
    @Enumerated(EnumType.STRING)
    Onset onset;
    Long duration;
    @Enumerated(EnumType.STRING)
    DurationUnit durationUnit;
    @Enumerated(EnumType.STRING)
    UlcerType ulcerType;
    String otherUlcerType;

    boolean sfl;
    boolean evlt;
    boolean spd;
    boolean dm;
    Long dmDuration;
    DurationUnit dmDurationUnit;
    boolean dietControl;
    boolean oha;
    boolean insulin;

    boolean femoralPulseRight;
    boolean poplitealPulseRight;
    boolean ptaPulseRight;
    boolean dpaPulseRight;

    boolean femoralPulseLeft;
    boolean poplitealPulseLeft;
    boolean ptsPulseLeft;
    boolean dpaPulseLeft;

    boolean pvd;
    boolean intermittentClaudication;
    boolean restPain;
    Laterality restPainLaterality;
    boolean neuropathy;
    Double monofilamentRight;
    Double monofilamentLeft;
    boolean footDeformities;
    boolean highArchRight;
    boolean flatFootRight;
    boolean clawToesRight;
    boolean halluxValgusRight;
    boolean charcotsFootRight;
    boolean highArchLeft;
    boolean flatFootLeft;
    boolean clawToesLeft;
    boolean halluxValgusLeft;
    boolean charcotsFootLeft;

    boolean dyslipidaemia;
    boolean hypertension;
    boolean smoking;
    boolean cardiacDisease;
    boolean cerebralVascularAccidents;
    boolean woundBiopsy;
    @Lob
    String woundBiopsyDetails;

    /**
     *
     *
     *
     *
     *
     * PVD	Yes	No Intermittent claudication	Yes	No Rest pain Yes	No Right / Left
     * / Bi lateral Neuropathy yes	No	(if yes) Monofilament test	R - L- Foot
     * deformities
     *
     */
    @Transient
    boolean displayVenous;
    @Transient
    boolean displayDm;
    @Transient
    boolean displayIschaemic;

    public boolean isDisplayIschaemic() {
        if (ulcerType == null) {
            displayIschaemic = false;
            return displayIschaemic;
        } else {
            switch (ulcerType) {
                case Neuroischaemic_Ulcer:
                case Neuropathic_Ulcer:
                case Ischaemic_Ulcer:
                    displayIschaemic = true;
                    break;
                default:
                    displayIschaemic = false;
            }
            return displayIschaemic;
        }
    }

    public boolean isDisplayDm() {
        if (ulcerType == null) {
            displayDm = false;
            return displayDm;
        } else {
            switch (ulcerType) {
                case Venous_Ulcer:
                case Neuroischaemic_Ulcer:
                case Neuropathic_Ulcer:
                case Ischaemic_Ulcer:
                    displayDm = true;
                    break;
                default:
                    displayDm = false;
            }
            return displayDm;
        }
    }

    public void setDisplayDm(boolean displayDm) {
        this.displayDm = displayDm;
    }

    public boolean isDisplayVenous() {
        if (ulcerType == null) {
            displayVenous = false;
        } else {
            displayVenous = ulcerType.equals(UlcerType.Venous_Ulcer);
        }
        return displayVenous;
    }

    public void setDisplayVenous(boolean displayVenous) {
        this.displayVenous = displayVenous;
    }

    public boolean isDyslipidaemia() {
        return dyslipidaemia;
    }

    public void setDyslipidaemia(boolean dyslipidaemia) {
        this.dyslipidaemia = dyslipidaemia;
    }

    public boolean isHypertension() {
        return hypertension;
    }

    public void setHypertension(boolean hypertension) {
        this.hypertension = hypertension;
    }

    public boolean isSmoking() {
        return smoking;
    }

    public void setSmoking(boolean smoking) {
        this.smoking = smoking;
    }

    public boolean isCardiacDisease() {
        return cardiacDisease;
    }

    public void setCardiacDisease(boolean cardiacDisease) {
        this.cardiacDisease = cardiacDisease;
    }

    public boolean isCerebralVascularAccidents() {
        return cerebralVascularAccidents;
    }

    public void setCerebralVascularAccidents(boolean cerebralVascularAccidents) {
        this.cerebralVascularAccidents = cerebralVascularAccidents;
    }

    public boolean isWoundBiopsy() {
        return woundBiopsy;
    }

    public void setWoundBiopsy(boolean woundBiopsy) {
        this.woundBiopsy = woundBiopsy;
    }

    public String getWoundBiopsyDetails() {
        return woundBiopsyDetails;
    }

    public void setWoundBiopsyDetails(String woundBiopsyDetails) {
        this.woundBiopsyDetails = woundBiopsyDetails;
    }

    public boolean isFemoralPulseRight() {
        return femoralPulseRight;
    }

    public void setFemoralPulseRight(boolean femoralPulseRight) {
        this.femoralPulseRight = femoralPulseRight;
    }

    public boolean isPoplitealPulseRight() {
        return poplitealPulseRight;
    }

    public void setPoplitealPulseRight(boolean poplitealPulseRight) {
        this.poplitealPulseRight = poplitealPulseRight;
    }

    public boolean isPtaPulseRight() {
        return ptaPulseRight;
    }

    public void setPtaPulseRight(boolean ptaPulseRight) {
        this.ptaPulseRight = ptaPulseRight;
    }

    public boolean isDpaPulseRight() {
        return dpaPulseRight;
    }

    public void setDpaPulseRight(boolean dpaPulseRight) {
        this.dpaPulseRight = dpaPulseRight;
    }

    public boolean isFemoralPulseLeft() {
        return femoralPulseLeft;
    }

    public void setFemoralPulseLeft(boolean femoralPulseLeft) {
        this.femoralPulseLeft = femoralPulseLeft;
    }

    public boolean isPoplitealPulseLeft() {
        return poplitealPulseLeft;
    }

    public void setPoplitealPulseLeft(boolean poplitealPulseLeft) {
        this.poplitealPulseLeft = poplitealPulseLeft;
    }

    public boolean isPtsPulseLeft() {
        return ptsPulseLeft;
    }

    public void setPtsPulseLeft(boolean ptsPulseLeft) {
        this.ptsPulseLeft = ptsPulseLeft;
    }

    public boolean isDpaPulseLeft() {
        return dpaPulseLeft;
    }

    public void setDpaPulseLeft(boolean dpaPulseLeft) {
        this.dpaPulseLeft = dpaPulseLeft;
    }

    public boolean isPvd() {
        return pvd;
    }

    public void setPvd(boolean pvd) {
        this.pvd = pvd;
    }

    public boolean isIntermittentClaudication() {
        return intermittentClaudication;
    }

    public void setIntermittentClaudication(boolean intermittentClaudication) {
        this.intermittentClaudication = intermittentClaudication;
    }

    public boolean isRestPain() {
        return restPain;
    }

    public void setRestPain(boolean restPain) {
        this.restPain = restPain;
    }

    public Laterality getRestPainLaterality() {
        return restPainLaterality;
    }

    public void setRestPainLaterality(Laterality restPainLaterality) {
        this.restPainLaterality = restPainLaterality;
    }

    public boolean isNeuropathy() {
        return neuropathy;
    }

    public void setNeuropathy(boolean neuropathy) {
        this.neuropathy = neuropathy;
    }

    public Double getMonofilamentRight() {
        return monofilamentRight;
    }

    public void setMonofilamentRight(Double monofilamentRight) {
        this.monofilamentRight = monofilamentRight;
    }

    public Double getMonofilamentLeft() {
        return monofilamentLeft;
    }

    public void setMonofilamentLeft(Double monofilamentLeft) {
        this.monofilamentLeft = monofilamentLeft;
    }

    public boolean isFootDeformities() {
        return footDeformities;
    }

    public void setFootDeformities(boolean footDeformities) {
        this.footDeformities = footDeformities;
    }

    public boolean isHighArchRight() {
        return highArchRight;
    }

    public void setHighArchRight(boolean highArchRight) {
        this.highArchRight = highArchRight;
    }

    public boolean isFlatFootRight() {
        return flatFootRight;
    }

    public void setFlatFootRight(boolean flatFootRight) {
        this.flatFootRight = flatFootRight;
    }

    public boolean isClawToesRight() {
        return clawToesRight;
    }

    public void setClawToesRight(boolean clawToesRight) {
        this.clawToesRight = clawToesRight;
    }

    public boolean isHalluxValgusRight() {
        return halluxValgusRight;
    }

    public void setHalluxValgusRight(boolean halluxValgusRight) {
        this.halluxValgusRight = halluxValgusRight;
    }

    public boolean isCharcotsFootRight() {
        return charcotsFootRight;
    }

    public void setCharcotsFootRight(boolean charcotsFootRight) {
        this.charcotsFootRight = charcotsFootRight;
    }

    public boolean isHighArchLeft() {
        return highArchLeft;
    }

    public void setHighArchLeft(boolean highArchLeft) {
        this.highArchLeft = highArchLeft;
    }

    public boolean isFlatFootLeft() {
        return flatFootLeft;
    }

    public void setFlatFootLeft(boolean flatFootLeft) {
        this.flatFootLeft = flatFootLeft;
    }

    public boolean isClawToesLeft() {
        return clawToesLeft;
    }

    public void setClawToesLeft(boolean clawToesLeft) {
        this.clawToesLeft = clawToesLeft;
    }

    public boolean isHalluxValgusLeft() {
        return halluxValgusLeft;
    }

    public void setHalluxValgusLeft(boolean halluxValgusLeft) {
        this.halluxValgusLeft = halluxValgusLeft;
    }

    public boolean isCharcotsFootLeft() {
        return charcotsFootLeft;
    }

    public void setCharcotsFootLeft(boolean charcotsFootLeft) {
        this.charcotsFootLeft = charcotsFootLeft;
    }

    public boolean isDm() {
        return dm;
    }

    public void setDm(boolean dm) {
        this.dm = dm;
    }

    public Long getDmDuration() {
        return dmDuration;
    }

    public void setDmDuration(Long dmDuration) {
        this.dmDuration = dmDuration;
    }

    public DurationUnit getDmDurationUnit() {
        return dmDurationUnit;
    }

    public void setDmDurationUnit(DurationUnit dmDurationUnit) {
        this.dmDurationUnit = dmDurationUnit;
    }

    public boolean isDietControl() {
        return dietControl;
    }

    public void setDietControl(boolean dietControl) {
        this.dietControl = dietControl;
    }

    public boolean isOha() {
        return oha;
    }

    public void setOha(boolean oha) {
        this.oha = oha;
    }

    public boolean isInsulin() {
        return insulin;
    }

    public void setInsulin(boolean insulin) {
        this.insulin = insulin;
    }

    public boolean isSfl() {
        return sfl;
    }

    public void setSfl(boolean sfl) {
        this.sfl = sfl;
    }

    public boolean isEvlt() {
        return evlt;
    }

    public void setEvlt(boolean evlt) {
        this.evlt = evlt;
    }

    public boolean isSpd() {
        return spd;
    }

    public void setSpd(boolean spd) {
        this.spd = spd;
    }

    public String getOtherUlcerType() {
        return otherUlcerType;
    }

    public void setOtherUlcerType(String otherUlcerType) {
        this.otherUlcerType = otherUlcerType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Laterality getLaterality() {
        return laterality;
    }

    public void setLaterality(Laterality laterality) {
        this.laterality = laterality;
    }

    public Onset getOnset() {
        return onset;
    }

    public void setOnset(Onset onset) {
        this.onset = onset;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public DurationUnit getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(DurationUnit durationUnit) {
        this.durationUnit = durationUnit;
    }

    public UlcerType getUlcerType() {
        return ulcerType;
    }

    public void setUlcerType(UlcerType ulcerType) {
        this.ulcerType = ulcerType;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getInactivatedDate() {
        return inactivatedDate;
    }

    public void setInactivatedDate(Date inactivatedDate) {
        this.inactivatedDate = inactivatedDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Wound)) {
            return false;
        }
        Wound other = (Wound) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sss.wc.entity.Encounter[ id=" + id + " ]";
    }

}
