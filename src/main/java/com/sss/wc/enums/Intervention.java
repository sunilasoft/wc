/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sss.wc.enums;

/**
 *
 * @author buddhika_ari
 */
public enum Intervention {
    Limited_Debridement_without_Local,
    Limited_Debridement_with_Local,
    Detainment_in_Theatre;

    public String getLabel() {
        switch (this) {
            case Detainment_in_Theatre: return "Detainment in Theatre";
            case Limited_Debridement_with_Local: return "Limited Debridement with Local";
            case Limited_Debridement_without_Local: return "Limited Debridement without Local";
            default:
                return "Other";
        }

    }
}
