/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sss.wc.enums;

/**
 *
 * @author buddh
 */
public enum Privilege {
    Add_Patient,
    Search_patients,
    Edit_Patient,
    Delete_Patient,
    Manage_User,
    Manage_Institutions,
    Manage_Departments,
    Manage_Items,
    System_administration,
}
