/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sss.wc.enums;

/**
 *
 * @author server
 */
public enum UlcerType {
    Venous_Ulcer,
    Neuropathic_Ulcer,
    Neuroischaemic_Ulcer,
    Ischaemic_Ulcer,
    Vasculitis,
    Infections,
    Other,
}
