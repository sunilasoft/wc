# README #

This is a web based enterprise application to manage a wound clinic.

### What is this repository for? ###

* The source code of the web application is available 
* 1.0.2
* Javeloped with Netbeans

### How do I get set up? ###

* Install Netbeans JavaEE edition
* Install GlassFish 4 or above
* Install git and git-gui
* Install mySQL
* Clone with Netbeans or close with git and then open the project with Netbeans
* Configure the persistance.xml file to match the connection to a database
* Run the application


### Who do I talk to? ###

* Dr. M H B Ariyaratne
* (+94)71 58 123 99
* buddhika.ari@gmail.com